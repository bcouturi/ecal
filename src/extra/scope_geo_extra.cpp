/*****************************************************************************\
 * (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       */

/*

TEST Constructors, used in various compact files, for the sake of tests only.

*/
#include "DD4hep/DetFactoryHelper.h"
#include <iostream>

using namespace std;
using namespace dd4hep;

static Ref_t create_scope( Detector& description, xml_h e, SensitiveDetector sens ) {

  xml_det_t x_det    = e;
  string    det_name = x_det.nameStr();
  xml_dim_t dim      = x_det.dimensions();
  xml_dim_t pos      = x_det.position();

  double width     = dim.width();
  double height    = dim.height();
  double thickness = dim.thickness();
  double gap       = dim.gap();

  double x = pos.x();
  double y = pos.y();
  double z = pos.z();

  Material silicon     = description.material( "Silicon" );
  Material Vacuum      = description.material( "Vacuum" );
  int      layer_count = dim.number();
  double   interval    = gap + thickness;
  double   box_length  = ( layer_count - 1 ) * gap + thickness * layer_count;

  Box    envelope( width / 2, height / 2, box_length / 2 );
  Volume envelopeVol( det_name + "_envelope", envelope, Vacuum );

  sens.setType( "tracker" );
  DetElement sdet( det_name, x_det.id() );

  for ( int layer_num = 0; layer_num < layer_count; ++layer_num ) {
    string     layer_name = _toString( layer_num, "layer%d" );
    Volume     layer_vol( layer_name, Box( width / 2, height / 2, thickness / 2 ), silicon );
    DetElement sensor_det( sdet, layer_name, x_det.id() + layer_num + 1 );
    layer_vol.setSensitiveDetector( sens );

    // As the centre of the boxes is in the middle, we offset the sensor a half length plus half the sensor itself
    double       placement_offset = -box_length / 2 + thickness / 2;
    PlacedVolume tmp_pv =
        envelopeVol.placeVolume( layer_vol, Position( 0, 0, placement_offset + layer_num * interval ) );
    tmp_pv.addPhysVolID( "layer", sensor_det.id() );
    sensor_det.setPlacement( tmp_pv );
  }

  envelopeVol.setAttributes( description, x_det.regionStr(), x_det.limitsStr(), x_det.visStr() );
  Volume motherVol = description.pickMotherVolume( sdet );
  PlacedVolume phv = motherVol.placeVolume( envelopeVol, Position( x, y, z ) );
  phv.addPhysVolID( "system", sdet.id() );
  sdet.setPlacement( phv );
  return sdet;
}
DECLARE_DETELEMENT( TScope, create_scope )

static Ref_t create_scope_box( Detector& description, xml_h e, SensitiveDetector ) {
  xml_det_t x_det    = e;
  string    det_name = x_det.nameStr();
  xml_dim_t dim      = x_det.dimensions();
  xml_dim_t pos      = x_det.position();

  double width     = dim.width();
  double height    = dim.height();
  double length    = dim.length();
  double thickness = dim.thickness();

  double x = pos.x();
  double y = pos.y();
  double z = pos.z();

  Material Be     = description.material( "Be" );
  Material Vacuum = description.material( "Vacuum" );

  // Creating the boxes
  Volume extbox( det_name + "Ext", Box( width / 2, height / 2, length / 2 ), Be );
  Volume intbox( det_name + "_int", Box( width / 2 - thickness, height / 2 - thickness, length / 2 - thickness ),
                 Vacuum );

  // Placing the internal one, with Vacuum
  PlacedVolume intbox_placement = extbox.placeVolume( intbox, Position( 0, 0, 0 ) );

  // Setting the external box attributes from the COmpact XML
  extbox.setAttributes( description, x_det.regionStr(), x_det.limitsStr(), x_det.visStr() );

  // Creating the new detector element and associating it with the internal box
  DetElement det( det_name, x_det.id() );
  det.setPlacement( intbox_placement );

  // Now placing the external box
  Volume motherVol = description.pickMotherVolume( det );
  motherVol.placeVolume( extbox, Position( x, y, z ) );

  return det;
}
DECLARE_DETELEMENT( TScopeBox, create_scope_box )

/*
 * Instead of creating a bounding box for the sensor array as in create scope,
 * create them as an assembly
 */
static Ref_t create_scope_assembly_det( Detector& description, xml_h e, SensitiveDetector sens ) {

  sens.setType( "tracker" );

  xml_det_t x_det    = e;
  string    det_name = x_det.nameStr();
  xml_dim_t dim      = x_det.dimensions();
  xml_dim_t pos      = x_det.position();

  double width     = dim.width();
  double height    = dim.height();
  double thickness = dim.thickness();
  double gap       = dim.gap();

  double x = pos.x();
  double y = pos.y();
  double z = pos.z();

  Material silicon = description.material( "Silicon" );
  Material Vacuum  = description.material( "Vacuum" );

  int    sensor_count = dim.number();
  double interval     = gap + thickness;
  double box_length   = ( sensor_count - 1 ) * gap + thickness * sensor_count;

  // Creation of the assembly for all the volumes
  Assembly   sensor_array( det_name + "_assembly" );
  DetElement sdet( det_name, x_det.id() );
  // sensor_array.setAttributes(description, x_det.regionStr(), x_det.limitsStr(), x_det.visStr());

  // Iterating to create individual sensors and place them
  for ( int sensor_num = 0; sensor_num < sensor_count; ++sensor_num ) {
    string sensor_name = _toString( sensor_num, "layer%d" );

    Volume     sensor_vol( sensor_name, Box( width / 2, height / 2, thickness / 2 ), silicon );
    DetElement sensor_det( sdet, sensor_name, x_det.id() + sensor_num + 1 );
    sensor_vol.setSensitiveDetector( sens );

    // As the centre of the boxes is in the middle, we offset the sensor a half length plus half the sensor itself
    double       placement_offset = -box_length / 2 + thickness / 2;
    PlacedVolume pv =
        sensor_array.placeVolume( sensor_vol, Position( 0, 0, placement_offset + sensor_num * interval ) );
    pv.addPhysVolID( "layer", sensor_det.id() );
    sensor_det.setPlacement( pv );
  }

  Volume       motherVol = description.pickMotherVolume( sdet );
  PlacedVolume phv       = motherVol.placeVolume( sensor_array, Position( x, y, z ) );
  phv.addPhysVolID( "system", sdet.id() );
  sdet.setPlacement( phv );
  return sdet;
}
DECLARE_DETELEMENT( TScopeAssemblyDet, create_scope_assembly_det )
/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Framework include files
#include "Detector/Common/Keys.h"
#include "Detector/Common/indent.h"

#include "DD4hep/Alignments.h"
#include "DD4hep/AlignmentsPrinter.h"
#include "DD4hep/Conditions.h"
#include "DD4hep/ConditionsDebug.h"
#include "DD4hep/ConditionsMap.h"
#include "DD4hep/Printout.h"
#include "DD4hep/detail/AlignmentsInterna.h"

#include <map>
#include <sstream>
#include <vector>

/// Initialization of sub-classes
LHCb::Detector::detail::DeIOVObject::DeIOVObject( dd4hep::DetElement de, dd4hep::cond::ConditionUpdateContext& ctxt )
    :
#if !defined( DD4HEP_MINIMAL_CONDITIONS )
    ConditionObject( Keys::deKeyName )
    ,
#endif
    detector( de )
    , geometry( de.placement() ) {
  hash = dd4hep::ConditionKey::KeyMaker( de, Keys::deKey ).hash;
  data.bindExtern( this );
  dd4hep::Condition::detkey_type det_key = detector.key();
  dd4hep::ConditionKey::KeyMaker kalign( det_key, dd4hep::align::Keys::alignmentKey );
  detectorAlignment = ctxt.condition( kalign.hash );
  if ( detectorAlignment.isValid() ) { toLocalMatrix = detectorAlignment.worldTransformation().Inverse(); }
}

/// Printout method to stdout
void LHCb::Detector::detail::DeIOVObject::print( int indent, int flg ) const {
  std::string prefix = getIndentation( indent );
  dd4hep::printout( dd4hep::INFO, "DeIOV", "%s*========== Detector:%s", prefix.c_str(), detector.path().c_str() );
  dd4hep::printout( dd4hep::INFO, "DeIOV", "%s+ Name:%s Hash:%016lX Type:%s Flags:%08X IOV:%s", prefix.c_str(),
                    dd4hep::cond::cond_name( this ).c_str(), hash, is_bound() ? data.dataType().c_str() : "<UNBOUND>",
                    flags, iov ? iov->str().c_str() : "--" );
  if ( flg & BASICS ) {
    const dd4hep::DetElement::Children& c = detector.children();
    dd4hep::printout( dd4hep::INFO, "DeIOV", "%s+ Detector:%s #Dau:%d", prefix.c_str(), detector.name(),
                      int( c.size() ) );
  }
  if ( flg & DETAIL ) {
    dd4hep::printout( dd4hep::INFO, "DeIOV", "%s+  >> Alignment:%s", prefix.c_str(),
                      dd4hep::yes_no( detectorAlignment.isValid() ) );
    if ( detectorAlignment.isValid() ) {
      char                         txt1[64], txt2[64], txt3[64];
      std::stringstream            str;
      dd4hep::Alignment::Object*   ptr            = detectorAlignment.ptr();
      const dd4hep::AlignmentData& alignment_data = detectorAlignment.data();
      const dd4hep::Delta&         D              = alignment_data.delta;

      if ( D.hasTranslation() )
        ::snprintf( txt1, sizeof( txt1 ), "Tr: x:%g y:%g z:%g ", D.translation.x(), D.translation.Y(),
                    D.translation.Z() );
      else
        ::snprintf( txt1, sizeof( txt1 ), "Tr:    ------- " );
      if ( D.hasRotation() )
        ::snprintf( txt2, sizeof( txt2 ), "Rot: phi:%g psi:%g theta:%g ", D.rotation.Phi(), D.rotation.Psi(),
                    D.rotation.Theta() );
      else
        ::snprintf( txt2, sizeof( txt2 ), "Rot:   ------- " );
      if ( D.hasPivot() )
        ::snprintf( txt3, sizeof( txt3 ), "Rot: x:%g y:%g z:%g ", D.pivot.Vect().X(), D.pivot.Vect().Y(),
                    D.pivot.Vect().Z() );
      else
        ::snprintf( txt3, sizeof( txt3 ), "Pivot: ------- " );

      dd4hep::printout( dd4hep::INFO, "DeIOV", "%s+  >> Aligment [%p] Typ:%s \tData:(%11s-%8s-%5s)", prefix.c_str(),
                        detectorAlignment.ptr(), dd4hep::typeName( typeid( *ptr ) ).c_str(),
                        D.hasTranslation() ? "Translation" : "", D.hasRotation() ? "Rotation" : "",
                        D.hasPivot() ? "Pivot" : "" );
      if ( D.hasTranslation() || D.hasRotation() || D.hasPivot() ) {
        dd4hep::printout( dd4hep::INFO, "DeIOV", "%s+  >> Aligment-Delta %s %s %s", prefix.c_str(), txt1, txt2, txt3 );
      }
    }
  }
}

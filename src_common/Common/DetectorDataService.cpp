/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "TTimeStamp.h"

#include "DD4hep/ConditionDerived.h"
#include "DD4hep/DetFactoryHelper.h"
#include "DD4hep/InstanceCount.h"
#include "DD4hep/Printout.h"
#include "Detector/Common/DetectorDataService.h"

#include "DD4hep/ConditionDerived.h"
#include "DD4hep/Conditions.h"
#include "DD4hep/ConditionsMap.h"

#include "DDCond/ConditionsContent.h"
#include "DDCond/ConditionsDataLoader.h"
#include "DDCond/ConditionsManager.h"
#include "DDCond/ConditionsManagerObject.h"
#include "DDCond/ConditionsSlice.h"

#include "Conditions/ConditionsRepository.h"
#include "Detector/Common/DeAlignmentCall.h"
#include "Detector/Common/DeConditionCall.h"
#include "Detector/Common/Keys.h"

void LHCb::Detector::DetectorDataService::initialize( std::string loadingDir, std::string gitTag ) {
  m_manager["PoolType"]                 = "DD4hep_ConditionsLinearPool";
  m_manager["UserPoolType"]             = "DD4hep_ConditionsMapUserPool";
  m_manager["UpdatePoolType"]           = "DD4hep_ConditionsLinearUpdatePool";
  m_manager["LoaderType"]               = "LHCb_ConditionsLoader";
  m_manager["OutputUnloadedConditions"] = true;
  m_manager["LoadConditions"]           = true;
  m_manager.initialize();

  m_iov_typ = m_manager.registerIOVType( 0, "run" ).second;
  if ( 0 == m_iov_typ ) { dd4hep::except( "ConditionsPrepare", "++ Unknown IOV type supplied." ); }

  dd4hep::cond::ConditionsDataLoader& loader = m_manager.loader();
  // loader["ReaderType"]                       = "LHCb_ConditionsFileReader";
  // loader["Directory"]                        = loadingDir;
  // loader["Match"]                            = "file:";
  // loader["IOVType"]                          = "run";
  // loader.initialize();

  loader["ReaderType"]                       = "LHCb_ConditionsGitReader";
  loader["Directory"]                        = loadingDir;
  loader["DBTag"]                            = gitTag;
  loader["Match"]                            = "git://";
  loader["IOVType"]                          = "run";
  loader.initialize();

  for ( const auto& det : m_detectorNames ) {
    dd4hep::DetElement de       = m_description.detector( det );
    const auto&        de_conds = *de.extension<std::shared_ptr<ConditionsRepository>>();
    m_all_conditions->merge( *de_conds );
  }
}

void LHCb::Detector::DetectorDataService::finalize() {
  /// Clear it
  m_manager.clear();
  // Let's do the cleanup
  m_all_conditions.reset();
  m_manager.destroy();
}

std::shared_ptr<dd4hep::cond::ConditionsSlice> LHCb::Detector::DetectorDataService::get_slice( size_t iov ) {
  std::lock_guard<std::mutex>                    hold( m_cache_mutex );
  std::shared_ptr<dd4hep::cond::ConditionsSlice> sp = m_cache[iov];
  if ( !sp ) m_cache[iov] = sp = load_slice( iov );
  return sp;
}

std::shared_ptr<dd4hep::cond::ConditionsSlice> LHCb::Detector::DetectorDataService::load_slice( size_t iov ) {
  dd4hep::IOV req_iov( m_iov_typ, iov );
  auto        slice = std::make_shared<dd4hep::cond::ConditionsSlice>( m_manager, m_all_conditions );
  TTimeStamp  start;

  /// Load the conditions
  /// dd4hep::cond::ConditionsManager::Result total = manager.prepare(req_iov,*slice);
  dd4hep::cond::ConditionsManager::Result total = m_manager.load( req_iov, *slice );
  TTimeStamp                              comp;
  total += m_manager.compute( req_iov, *slice );
  TTimeStamp                                   stop;
  dd4hep::cond::ConditionsContent::Conditions& missing = slice->missingConditions();
  for ( const auto& m : missing ) {
    dd4hep::printout( dd4hep::ERROR, "DetectorDataService", "Failed to load condition [%016llX]: %s", m.first,
                      m.second->toString().c_str() );
  }
  dd4hep::printout( dd4hep::INFO, "Statistics",
                    "+  Created/Accessed a total of %ld conditions "
                    "(S:%6ld,L:%6ld,C:%6ld,M:%ld)  Load:%7.5f sec Compute:%7.5f sec",
                    total.total(), total.selected, total.loaded, total.computed, total.missing,
                    comp.AsDouble() - start.AsDouble(), stop.AsDouble() - comp.AsDouble() );
  return slice;
}

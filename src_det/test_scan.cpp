/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DD4hep/Printout.h"

#include "Detector/Scope/DeScope.h"
#include "Detector/Scope/ScopeService.h"
#include "TGeoManager.h"
#include "TGeoNavigator.h"

#include <iostream>

int main( int, char** ) {
  scope::ScopeService svc;
  svc.initialize( false, std::nullopt, true );

  auto   nav = gGeoManager->GetCurrentNavigator();
  double z   = -200;
  nav->SetCurrentPoint( 0, 0, z );
  nav->SetCurrentDirection( 0, 0, 1 );
  while ( z < 200 ) {
    std::ostringstream os;
    os  << z << " - " << nav->GetCurrentNode()->GetName() << " - "
              << nav->GetCurrentNode()->GetVolume()->GetMaterial()->GetName();
    dd4hep::printout( dd4hep::INFO, "test_scan", "%s", os.str().c_str());
    nav->FindNextBoundaryAndStep( 1000 );
    z = nav->GetCurrentPoint()[2];
  }
}
